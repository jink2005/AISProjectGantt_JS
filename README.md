# AISProjectGantt_JS
甘特图浏览工具

#### Description
基于dhtmlxGantt Standard Edition开发的甘特图，在https://github.com/waihaolaoxu/gantt继续开发。

开发目标为：
类似office一样的甘特图浏览工具：
基于JS的相关管理工具，通过读取本地文件构建和显示甘特图。
对甘特图进行编辑，导出到本地文件，以便分享和再次查看。

#### Software Architecture
基于dhtmlxGantt Standard Edition开发

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)