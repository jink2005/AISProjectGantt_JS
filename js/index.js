// gantt.message("Try to move or resize task to not working time");

/**
 * Marker
 */
// var date_to_str = gantt.date.date_to_str(gantt.config.task_date);
// var today = new Date(2018, 3, 5);
// gantt.addMarker({
//     start_date: today,
//     css: "today",
//     text: "收款",
//     title: "Today: " + date_to_str(today)
// });
//
// var start = new Date();
// gantt.addMarker({
//     start_date: start,
//     css: "today_line",
//     text: "今天",
//     title: "今天"
// });


// ganttFn.init({
//     id: 'gantt_here',
//     data: data_blank,
//     isPattern: true
// });

gantt.templates.scale_cell_class = function (date) {
    if (date.getDay() == 0 || date.getDay() == 6) {
        return "weekend";
    }
};
gantt.templates.task_cell_class = function (item, date) {
    if (date.getDay() == 0 || date.getDay() == 6) {
        return "weekend";
    }
};

gantt.config.auto_scheduling = true;
gantt.config.auto_scheduling_strict = true;

gantt.config.order_branch = true;
gantt.config.order_branch_free = true;

gantt.config.row_height = 24;
gantt.config.xml_date = "%Y-%m-%d %H:%i";

gantt.init("gantt_here");

// var tasks = gantt.getTaskByTime();
// var links = gantt.getLinks();
// console.log('tasks',tasks)
// console.log('links',links)

var fn = {
    //切换施工时间类型
    startType: function (e) {
        switch (e.value) {
            case '0':
                gantt.config.work_time = false;
                break;
            case '1':
                gantt.config.work_time = true;
                break;
        }
        gantt.render();
        gantt.batchUpdate(function () {
            gantt.eachTask(function (task) {
                gantt.updateTask(task.id);
            });
        });
    }
};

var fileDnD = fileDragAndDrop();
fileDnD.init(gantt.$container);
var form = document.getElementById("mspImport");

function importGantt() {
    var mspFile = document.getElementById("mspFile");
    mspFile.click();
}

function importSubmit() {
    var mspImportBtn = document.getElementById("mspImportBtn");
    mspImportBtn.click();
}

form.onsubmit = function (event) {
    event.preventDefault();
    var fileInput = document.getElementById("mspFile");
    sendFile(fileInput.files[0]);
};

function sendFile(file) {
    fileDnD.showUpload();
    upload(file, function () {
        fileDnD.hideOverlay();
    })
}

function upload(file, callback) {
    gantt.importFromMSProject({
        data: file,
        callback: function (project) {
            if (project) {
                gantt.clearAll();

                if (project.config.duration_unit) {
                    gantt.config.duration_unit = project.config.duration_unit;
                }

                gantt.parse(project.data);
            }

            if (callback)
                callback(project);
        }
    });
}

window.onbeforeunload = function () {
    return "请先保存AIS甘特图，防止数据丢失。离开?";
};